package conf

import (
	"encoding/json"
	"fmt"
	"sync"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var config *Config

func C() *Config {

	if config == nil {
		config = DefaultConfig()
	}
	return config
}

func DefaultConfig() *Config {
	return &Config{
		Application: &Application{
			Domain: "127.0.0.1",
		},
		MySQL: &MySQL{
			Host:     "192.168.87.128",
			Port:     3306,
			DB:       "vblog",
			Username: "wanye",
			Password: "wanye",
			Debug:    true,
		},
	}
}
func (c *Config) String() string {
	jd, err := json.MarshalIndent(c, "", "   ")
	if err != nil {
		return fmt.Sprintf("%p", c)
	}
	return string(jd)
}

type Config struct {
	Application *Application `toml:"app"`
	MySQL       *MySQL       `toml:"mysql"`
}

type Application struct {
	Domain string `toml:"domain" env:"APP_DOMAIN"`
}

type MySQL struct {
	//远程主机名
	Host string `toml:"host" env:"DATASOURCE_HOST"`
	//端口号
	Port int `toml:"port" env:"DATASOURCE_PORT"`
	//数据库名
	DB string `toml:"database" env:"DATASOURCE_DB"`
	// 用户名
	Username string `toml:"username" env:"DATASOURCE_USERNAME"`
	// 密码
	Password string `toml:"password" env:"DATASOURCE_PASSWORD"`
	// 调试模式
	Debug bool `toml:"debug" env:"DATASOURCE_DEBUG"`

	//判断这个私有属性，来判断是否返回已有对象

	db *gorm.DB
	l  sync.Mutex
}

func (m *MySQL) DSN() string {
	fmt.Printf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
}

// 通过配置就能获得一个DB实例
func (m *MySQL) GetDB() *gorm.DB {
	m.l.Lock()
	defer m.l.Unlock()
	if m.db == nil {
		db, err := gorm.Open(mysql.Open(m.DSN()), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		m.db = db
		// 补充debug配置
		if m.Debug {
			m.db = db.Debug()
		}
	}
	return m.db
}

// 配置对象提供全局单例配置
func (c *Config) DB() *gorm.DB {

	return c.MySQL.GetDB()

}
