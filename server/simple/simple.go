package simple

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/1027452269/simpleServer/server/apps/students"
	"gitlab.com/1027452269/simpleServer/server/apps/students/impl"
)

var (
	i   students.Service // 声明变量
	ctx = context.Background()
)

func OpenFile(filePath string) (string, error) {

	openFile, e := os.OpenFile(filePath, os.O_RDWR, 0777)

	if e != nil {
		fmt.Println(e)
		return "", errors.New("打开失败")
	}
	defer func() {
		openFile.Close()
	}()
	buf := make([]byte, 1024)
	total := 0
	for {
		len, _ := openFile.Read(buf)
		if len == 0 {
			break
		}
		total += len
		//fmt.Println(string(buf))
	}

	return string(buf[:total]), nil
}

func ServerStart() {

	// Hello world, the web server
	rpath, _ := os.Getwd()
	indexHandler := func(w http.ResponseWriter, req *http.Request) {
		indexFile, ok := OpenFile(rpath + "\\html" + "\\index.html")
		if ok != nil {
			indexFile = "404"
		}
		io.WriteString(w, indexFile)
	}
	studentsHandler := func(w http.ResponseWriter, req *http.Request) {

		//打开数据库查询
		set, err := i.QueryStudent(ctx, students.NewQueryStudentRequest())
		if err != nil {
			fmt.Println("students查询出错")
			io.WriteString(w, "服务器内部错误")
		}
		io.WriteString(w, set.String())
	}
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/index.html", indexHandler)
	http.HandleFunc("/students", studentsHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))

}
func init() {
	i = impl.NewStudentServiceImpl()
}
