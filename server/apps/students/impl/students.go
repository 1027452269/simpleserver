package impl

import (
	"context"

	"gitlab.com/1027452269/simpleServer/server/apps/students"
)

var _ students.Service = (*StudentServiceImpl)(nil)

// 创建 学生

func (i *StudentServiceImpl) CreateStudent(ctx context.Context, in *students.CreateStudentRequest) (*students.Student, error) {
	// 1. 校验用户参数请求
	if err := in.Validate(); err != nil {
		return nil, err
	}
	// 2.创建student实例对象
	stu := students.NewStudent(in)

	// 3.把对象持久化到数据库里
	if err := i.db.WithContext(ctx).Model(&stu).Create(stu).Error; err != nil {
		return nil, err
	}

	// 4.返回创建好的对象
	return stu, nil

}
func (i *StudentServiceImpl) QueryStudent(ctx context.Context, in *students.QueryStudentRequest) (*students.StudentSet, error) {

	// 构造一个mysql 条件查询语句 select * from students where ....
	query := i.db.WithContext(ctx).Model(&students.Student{})

	// 构造条件 where username = ""
	if in.Name != "" {
		query = query.Where("name = ? ", in.Name)
	}
	set := students.NewStudentSet()
	err := query.Count(&set.Total).Error
	if err != nil {
		return nil, err
	}

	err = query.Limit(in.Limit()).Offset(in.Offset()).Find(&set.Items).Error
	if err != nil {
		return nil, err
	}
	return set, nil
}

func (i *StudentServiceImpl) DescribeStudent(ctx context.Context, in *students.DescribeStudentRequest) (*students.Student, error) {

	// 构造一个mysql 条件查询语句 select * from students where ....
	query := i.db.WithContext(ctx).Model(&students.Student{}).Where(" id = ? ", in.StudentId)
	stu := students.NewStudent(&students.CreateStudentRequest{})
	if err := query.First(stu).Error; err != nil {
		return nil, err
	}
	return stu, nil
}
