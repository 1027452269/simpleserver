package impl

import (
	"gitlab.com/1027452269/simpleServer/server/conf"
	"gorm.io/gorm"
)

type StudentServiceImpl struct {

	// 依赖了一个数据库连接池对象
	db *gorm.DB
}

func NewStudentServiceImpl() *StudentServiceImpl {
	return &StudentServiceImpl{
		db: conf.C().DB(),
	}
}
