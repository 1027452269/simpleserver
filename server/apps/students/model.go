package students

import (
	"time"

	"github.com/infraboard/mcube/tools/pretty"
)

type Student struct {
	Id        string `json:"id" gorm:"column:id"`
	CreatedAt int64  `json:"created_at" gorm:"column:created_at"`
	*CreateStudentRequest
}

func NewStudent(req *CreateStudentRequest) *Student {
	return &Student{
		CreatedAt:            time.Now().Unix(),
		CreateStudentRequest: req,
	}
}
func (stu *Student) TableName() string {
	return "students"
}

func (stu *Student) String() string {
	return pretty.ToJSON(stu)
}
