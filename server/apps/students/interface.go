package students

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/tools/pretty"
)

const (
	AppName = "students"
)

var (
	v = validator.New()
)

type Service interface {
	CreateStudent(context.Context, *CreateStudentRequest) (*Student, error) //创建学生请求

	QueryStudent(context.Context, *QueryStudentRequest) (*StudentSet, error) //学生查询请求

	DescribeStudent(context.Context, *DescribeStudentRequest) (*Student, error) //学生描述请求

}

type CreateStudentRequest struct {
	Name string `json:"name" validate:"required" gorm:"column:name"`
	Age  int    `json:"age" validate:"required" gorm:"column:age"`
}

func NewCreateStudentRequest() *CreateStudentRequest {
	return &CreateStudentRequest{}
}

// 校验
func (c *CreateStudentRequest) Validate() error {
	return v.Struct(c)
}

type QueryStudentRequest struct {
	// 页大小
	PageSize int
	// 页码
	PageNumber int
	// 用户名
	Name string
}

func NewQueryStudentRequest() *QueryStudentRequest {
	return &QueryStudentRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

// 获取页容量
func (req *QueryStudentRequest) Limit() int {
	return req.PageSize
}

// 返回数据偏移量
func (req *QueryStudentRequest) Offset() int {
	return (req.PageNumber - 1) * req.PageSize
}

type DescribeStudentRequest struct {
	StudentId string
}

func NewDescribeStudentRequest(stuid string) *DescribeStudentRequest {
	return &DescribeStudentRequest{
		StudentId: stuid,
	}
}

type StudentSet struct {
	Total int64
	Items []*Student
}

func NewStudentSet() *StudentSet {
	return &StudentSet{
		Items: []*Student{},
	}
}
func (stu *StudentSet) String() string {
	return pretty.ToJSON(stu)
}
