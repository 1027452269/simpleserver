create table `students`(
    `id`  varchar(30) PRIMARY KEY,
    `created_at` int,
    `name` varchar(30)  CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `age` tinyint  NOT NULL  
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

insert into students values ("2024-1B-001",UNIX_TIMESTAMP(),"赵存浩",33);
insert into students values ("2024-1B-002",UNIX_TIMESTAMP(),"赵二",33);
insert into students values ("2024-1B-003",UNIX_TIMESTAMP(),"赵三",33);
insert into students values ("2024-1B-004",UNIX_TIMESTAMP(),"赵四",33);
insert into students values ("2024-1B-005",UNIX_TIMESTAMP(),"赵五",33);
insert into students values ("2024-1B-006",UNIX_TIMESTAMP(),"赵六",33);